---
title: "Getting Started"
date: 2010-01-27T13:08:37Z
---

In this chapter we you will learn how to install and use JFCM library in your application.

## Requirements

### Users

Ther requirements for JFCM **users** are minimal:

-   **Java Runtime or JDK 1.5** (also known as 5.0) and above.

Disk occupation is also very small: **less than 60 KB for the library jar**. This makes it possible to embed it in any project or enviroment (even smaller ones) without fear of library or JDK conflicts.

### Developers

To build JFCM, developers will need:

-   **JDK 1.5** or above, althought **1.7 is highly recommended**;
-   **Maven 3.x**:  
    [http://maven.apache.org/](http://maven.apache.org/)

[‹ Getting started](/book/documentation/getting-started "Go to previous page") [up](/book/documentation/getting-started "Go to parent page") [Download and installation ›](/book/documentation/getting-started/download-and-installation "Go to next page")



### Download JFCM

JFCM releases are hosted on Sourceforge, the download page can be found here:

[http://sourceforge.net/projects/jfcm/files/](http://sourceforge.net/projects/jfcm/files/ "http://sourceforge.net/projects/jfcm/files/")

### Installation

To use JFCM in your project you must **make library available in the classpath**, i.e. `jfcm-core-1.x.x.jar`, where _1.x.x_ is the version number.

### Using Ant

\[TODO: add Ant instructions\]

### Using Maven - method 1: JitPack.io

jfcm-core is not yet hosted on Maven Central repository. An easy way to include it in your project is to use [**JitPack.io**](https://jitpack.io/), an easy to use git package repository.

This is an example pom.xml for Maven:

<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 [http://maven.apache.org/xsd/maven-4.0.0.xsd">](http://maven.apache.org/xsd/maven-4.0.0.xsd"> "http://maven.apache.org/xsd/maven-4.0.0.xsd">")
    <modelVersion>4.0.0</modelVersion>

    <groupId>my.group.id</groupId>
    <artifactId>jfcm-example</artifactId>
    <version>1.0-SNAPSHOT</version>

    <repositories>
        <repository>
            <id>jitpack.io</id>
            <url>https://jitpack.io</url>
        </repository>
    </repositories>

    <dependencies>
        <dependency>
            <groupId>com.github.megadix</groupId>
            <artifactId>jfcm</artifactId>
            <version>v1.4.2</version>
        </dependency>
    </dependencies>
</project>

### Using Maven - method 2: install downloaded files into local repository

You can also install jfcm-core in your locale Maven repository:

-   download jfcm-core;
-   keep track of the version number: if the jar file you downloaded is named **`jfcm-core-1.4.1.jar`**, then the version number is _1.4.1_;
-   **install the library in your local Maven repository** with this command:

mvn install:install-file -DgroupId=org.megadix.jfcm
  -DartifactId=jfcm-core -Dversion=**CURRENT_VERSION** -Dpackaging=jar
  -Dfile=**PATH\_TO\_JAR**

Where:

-   CURRENT_VERSION is the version number of the library (see above);
-   PATH\_TO\_JAR is the absolute path to the jar file.

Now add the correct dependency:

    <dependency>
      <groupId>org.megadix.jfcm</groupId>
      <artifactId>jfcm-core</artifactId>
      <version>1.4.1</version>
    <dependency>

in the <dependencies> section of your _pom.xml_ file.

### Using Maven - method 3: build and install from source code

The last method is to build and install from source code:

-   Maven 3.x is needed;
-   download the **_project_** distribution, it's the archive named _jfcm-core-1.x.x-project.zip_;
-   extract files on your computer;
-   run this command from project root:  
    `mvn install`

After the build ("Build successful" message) you should have JFCM ﻿core installed ﻿in your repository.

[‹ Requirements](/book/documentation/getting-started/requirements "Go to previous page") [up](/book/documentation/getting-started "Go to parent page") [Your first fuzzy cognitive map ›](/book/documentation/getting-started/your-first-fuzzy-cognitive-map "Go to next page")



## Your first fuzzy cognitive map

Our first map is taken from:

_"Reti neuronali - dal perceptron alle reti caotiche e neuro-fuzzy"_  
Silvio Cammarata - Etas Libri

You can find the source code of this example in the samples package:

[http://jfcm.svn.sourceforge.net/viewvc/jfcm/JFCM-samples/trunk/](http://jfcm.svn.sourceforge.net/viewvc/jfcm/JFCM-samples/trunk/)

in `[InvestmentsExample.java](http://jfcm.svn.sourceforge.net/viewvc/jfcm/JFCM-samples/trunk/src/main/java/org/megadix/jfcm/samples/InvestmentsExample.java?view=log)`.

### The Map

In this cognitive map we have four **concepts**:

-   **c1:** Interest rate;
-   **c2:** Productive investments;
-   **c3:** Occupation;
-   **c4:** Inflation.

 Next, we'll identify some **connections** between these concepts:

-   an increase of _Interest rate_ results in a decrease of _Productive investments_ with a degree of -80%;
-   an increase of _Productive investments_ results in an increase of _Occupation_ with a degree of +100%;
-   an increase of _Occupation_ results in an increase of _Inflation_ with a degree of +90%;
-   an increase of _Inflation_ results in an increase of _Interest rate_ with a degree of +100%.

These values are taken from experience, interviews with experts, custom research, and so on. The topic itself is huge and interesting, but we can leave it by now. So, here's a nice picture of the conceptual map:

![Fuzzy Cognitive Map: Investments example](/img/documentation/InvestmentsExample.png)

Conceptual maps can be created in two ways:

-   programmatically, i.e. using JFCM APIs;
-   creating an xml file that describes the map and load it using the `org.megadix.jfcm.utils.FcmIO` utility class.

### How to create a map programmatically

The code below is an excerpt from **[jfcm-samples](https://github.com/megadix/jfcm-samples)**, a project on GitHub that contains this and other examples of JFCM usage.

The complete code for this particular example can be found in **[InvestmentsExample.java](https://github.com/megadix/jfcm-samples/blob/master/src/main/java/org/megadix/jfcm/samples/InvestmentsExample.java)** source file.

```java
CognitiveMap map = new CognitiveMap("Investments");
SignumActivator af = new SignumActivator();
af.setIncludePreviousOutput(false);

Concept c1 = new Concept("c1", "Interest rate", af, 0.0, 0.0, false);
map.addConcept(c1);

Concept c2 = new Concept("c2", "Productive investments", af, 0.0, 0.0, false);
map.addConcept(c2);

Concept c3 = new Concept("c3", "Occupation", af, 0.0, 0.0, false);
map.addConcept(c3);

Concept c4 = new Concept("c4", "Inflation", af, 0.0, 0.0, false);
map.addConcept(c4);

FcmConnection conn_1 = new WeightedConnection("c1 -> c2", "Interest rate -> Productive investments", -0.8);
map.addConnection(conn_1);
FcmConnection conn_2 = new WeightedConnection("c2 -> c3", "Productive investments -> Occupation", 1.0);
map.addConnection(conn_2);
FcmConnection conn_3 = new WeightedConnection("c3 -> c4", "Occupation -> Inflation", 0.9);
map.addConnection(conn_3);
FcmConnection conn_4 = new WeightedConnection("c4 -> c1", "Inflation -> Interest rate", 1.0);
map.addConnection(conn_4);

map.connect("c1", "c1 -> c2", "c2");
map.connect("c2", "c2 -> c3", "c3");
map.connect("c3", "c3 -> c4", "c4");
map.connect("c4", "c4 -> c1", "c1");
```

Configure concepts as "fixed":

```java
map.getConcepts().get("c1").setFixedOutput(true);
```

Or simply execute the map:

```java
map.execute();
```

Concepts are stored in a `java.util.Map`, indexed by concept name; this means that each **concept must have a unique name**, otherwise strange errors would start to happen! The concept Map can be retrieved with the `CognitiveMap.getConcepts()` method.

### How to create a map from an xml file

Here is the xml file that represents the above map.

Important Note: this example uses the **new 1.1 XML format**, used since version _1.1.0_ of the library.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<jfcm:maps xmlns:jfcm="http://www.megadix.org/standards/JFCM-map-v-1.2.xsd">
  <map name="Investments">
    <description>Example inspired by S. Cammarata "Reti neuronali - dal perceptron alle reti caotiche e neuro-fuzzy" (1997) Etas Libri</description>
    <concepts>
      <concept act="SIGNUM" input="0.0" name="c1" output="0.0">
        <description>Interest rate</description>
        <params>
          <param name="includePreviousOutput" value="false" />
        </params>
      </concept>
      <concept act="SIGNUM" input="0.0" name="c2" output="0.0">
        <description>Productive investments</description>
        <params>
          <param name="includePreviousOutput" value="false" />
        </params>
      </concept>
      <concept act="SIGNUM" input="0.0" name="c3" output="0.0">
        <description>Occupation</description>
        <params>
          <param name="includePreviousOutput" value="false" />
        </params>
      </concept>
      <concept act="SIGNUM" input="0.0" name="c4" output="0.0">
        <description>Inflation</description>
        <params>
          <param name="includePreviousOutput" value="false" />
        </params>
      </concept>
    </concepts>
    <connections>
      <connection from="c1" name="c1 -&gt; c2" to="c2" type="WEIGHTED">
        <description>Interest rate -&gt; Productive investments</description>
        <params>
          <param name="weight" value="-0.8" />
        </params>
      </connection>
      <connection from="c2" name="c2 -&gt; c3" to="c3" type="WEIGHTED">
        <description>Productive investments -&gt; Occupation</description>
        <params>
          <param name="weight" value="1.0" />
        </params>
      </connection>
      <connection from="c3" name="c3 -&gt; c4" to="c4" type="WEIGHTED">
        <description>Occupation -&gt; Inflation</description>
        <params>
          <param name="weight" value="0.9" />
        </params>
      </connection>
      <connection from="c4" name="c4 -&gt; c1" to="c1" type="WEIGHTED">
        <description>Inflation -&gt; Interest rate</description>
        <params>
          <param name="weight" value="1.0" />
        </params>
      </connection>
    </connections>
  </map>
</jfcm:maps>
```

Save it, then call `FcmIO.loadXml()`:

```
CognitiveMap map = FcmIO.loadXml("/PATH/TO/InvestmentsExample.xml").get(0);
```

`/PATH/TO/InvestmentsExample.xml` is the absolute or relative path to the XML file shown above. Because the file contains just one map, we call `get(0)`` on the returned `List` object to retrieve the first map (Lists positions in Java are 0-based).
