---
title: "Activation functions"
date: 2010-01-27T13:08:37Z
---

The concept of activation function has been borrowed from neural networks. Basically, it is a function that calculates the output of a concept based on its inputs, usually on the total sum. JFCM implements many of the most used activation functions found in literature, below we can see the activation charts of them.

`CauchyActivator`

![Chart of CauchyActivator activator](/img/documentation/CauchyActivator.png)

`GaussianActivator`

![Chart of GaussianActivator activator](/img/documentation/GaussianActivator.png)

`HyperbolicTangentActivator`

![Chart of HyperbolicTangentActivator activator](/img/documentation/HyperbolicTangentActivator.png)

`IntervalActivator`

![Chart of IntervalActivator activator](/img/documentation/IntervalActivator.png)

`LinearActivator`

![Chart of LinearActivator activator](/img/documentation/LinearActivator.png)

`NaryActivator`

![Chart of NaryActivator activator](/img/documentation/NaryActivator.png)

`SigmoidActivator`

![Chart of SigmoidActivator activator](/img/documentation/SigmoidActivator.png)

`SignumActivator`

![Chart of SignumActivator activator](/img/documentation/SignumActivator.png)
