---
title: "New Release 1.2 - bugfixes and new features"
date: 2012-05-25T15:31:44Z
---
New version **1.2** is out, with some great new features like **time-delay in `WeightedConnection`**. Here's the list of fixes/enhancements:

- [#12](https://sourceforge.net/apps/trac/jfcm/ticket/12) map.execute() should really have two phases
- [#13](https://sourceforge.net/apps/trac/jfcm/ticket/13) Add method CognitiveMap.reset() that resets every Concept to null
- [#14](https://sourceforge.net/apps/trac/jfcm/ticket/14) Create a deep copy constructor on CognitiveMap
- [#2](https://sourceforge.net/apps/trac/jfcm/ticket/2) XML output from FcmIO.saveAsXml() should include namespace
- [#15](https://sourceforge.net/apps/trac/jfcm/ticket/15) Backport LinearConceptActivator from 2.0-SNAPSHOT
- [#7](https://sourceforge.net/apps/trac/jfcm/ticket/7) Backport time delay to WeightedConnection from 2.0-SNAPSHOT
