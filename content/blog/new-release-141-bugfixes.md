---
title: "New release: 1.4.1 - bugfixes"
date: 2012-11-22T16:13:38+02:00
---

Version **1.4.1** is out, it contains these fixes:

-   fixed bug in `FcmIO.saveAsXml()`: wrong schema version in XML output, uses 1.1 but should be 1.2;
-   Refactored `FcmIO.saveAsXml()` to `ToXmlVisitor.java`
