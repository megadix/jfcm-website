---
title: "Cogtrust Project Uses Jfcm"
date: 2011-10-26T13:32:33Z
categories: ["News"]
tags: ["tools", "libraries", "fuzzy logic", "projects", "Cog-Trust", "sociology", "artificial intelligence", "cnr"]
---

[Cog-Trust](http://t3.istc.cnr.it/trustwiki/index.php/Cog-Trust) is an interesting project that mixes _sociology_, _artificial intelligence_ and _information technology_ to provide an environment where different delegation strategies can be tested and analyzed using software _agents_.

We have planned an interview with the authors, in the mean time don't miss the project's website:

[Cog-Trust Sourceforge Page](https://sourceforge.net/projects/mindraces-bdi/)
