---
title: "Intervista CogTrust (Italiano)"
date: 2011-12-15T13:37:45Z
categories: ["News"]
tags: ["tools", "libraries", "fuzzy logic", "projects", "Cog-Trust", "sociology", "artificial intelligence", "cnr", "interview"]
---

[_CogTrust_](http://t3.istc.cnr.it/trustwiki/index.php/Cog-Trust) è un progetto di ricerca che utilizza JFCM per la parte "intelligente" del loro modello, e rappresenta uno degli esempi di come, con la multi-disciplinarietà e l'utilizzo di strumenti e tecnologie esistenti open source, si possano ottenere grandi risultati, contribuendo al miglioramento degli strumenti stessi.

Leggendo le loro risposte da sviluppatore e "papà" di JFCM mi sono sentito molto orgoglioso, ma soprattutto mi prudono le mani perché mi hanno fatto venire in mente un sacco di idee nuove!

[![](/system/files/u1/gb.png)Click here to find the English Translation](http://jfcm.megadix.it/blog/cogtrust-interview-english)

### L'intervista

**JFCM** Partiamo subito con le presentazioni. Parlateci un po' del vostro team: dove e come è nato, le esperienze dei singoli membri, i progetti in corso.

**CogTrust** Il nostro team si chiama T3: [Trust, Theory and Technology (t3.istc.cnr.it)](http://t3.istc.cnr.it/), ed e' un gruppo di ricerca collocato all'interno dell'Istituto di Scienze e Technologie Cognitive (ISTC, [www.istc.cnr.it](http://www.istc.cnr.it "www.istc.cnr.it")) del CNR di Roma. T3 ha come interesse la **ricerca su fiducia (Trust) all'interno di domini sociali, con particolare attenzione per lo studio di modelli socio-cognitivi che ruotano attorno al concetto di Trust**.

Nato intorno all anno 2000, il gruppo vanta collaborazioni con ricercatori di provenienza internazionale con background in vari settori scentifici, come Psicologia, Sociologia, Scienze dell'Informazione e Intelligenza Artificiale. Il gruppo ha ormai un ruolo riconosciuto all'interno della comunità scientifica internazionale, testimoniato dalle oltre 100 pubblicazioni sui piu importanti journal e nelle maggiori conferenze leader nel settore. Inoltre, il gruppo e' anche attivo in varie iniziative al servizio della comunità, con workshop ed attività editoriali, nell'obiettivo stimolare e supportare la ricerca su Trust.

**JFCM** Che cos'è esattamente il progetto CogTrust?

**CogTrust** **[_CogTrust_](http://t3.istc.cnr.it/trustwiki/index.php/Cog-Trust) e' un progetto software sviluppato all'interno della ricerca su modelli computazionali per processi socio-cognitivi basati su fiducia.** CogTrust implementa un modello di valutazione di fiducia basato su ruoli e categorie che si fonda sulla teoria di fiducia socio-cognitiva di Falcone et al. Questa teoria spiega come la fiducia nasce e si sviluppa all'interno della mente di un agente cognitivo e come questa abbia un ruolo cruciale all'interno di processi di delegazione e cooperazione sociale (per dettagli, _Trust Theory: A Socio-Cognitive and Computational Model; Castelfranchi, Falcone, Wiley, 2010_).

Per i suoi scopi, CogTrust realizza un' infrastruttura per simulare di un sistema sociale in cui agenti (trustor) cercano di eseguire i propri task avvalendosi della cooperazione e della delega. Il sistema studia il decision making basato su fiducia, ovvero come la scelta di delegare un certo tipo di attivita possa avvenire su agenti scelti tra di una popolazione di possibili esecutori (trustee). Claim del modello è che la valutazione dei criteri della scelta di delega avvenga per mezzo di strategie basate sul concetto di fiducia (trust), influenzata da meccanismi di categorizzazione, valutazione dei rischi e dei vari contesti operativi. In questo, CogTrust è un meccanismo decisionale che consente l'esplorazione l'uso del' informazione via via memorizzabile e ricercabile anche da semplici "robot" software

**JFCM** Che tecnologie avete scelto per realizzarlo?

**CogTrust** CogTrust e' un progetto interamente open source. Tecnologia collante di tutti i moduli è Java ma in CogTrust vengono integrate varie librerie e tecniche di programmazione avanzata. Nello specifico, CogTrust realizza un sistema Multi Agente: [_Jason_](http://jason.sourceforge.net/) è il linguaggio utilizzato per i meccanismi deliberativi degli agenti, [_CartAgO_](http://cartago.sourceforge.net/) è la piattaforma usata per la programmazione dell'ambiente di simulazione, [JGraph](http://www.jgraph.com/) per la parte grafica di analisi e visualizzazione dei risultati attraverso grafi interattivi, ed infine [JFCM](http://jfcm.megadix.it/) per l'implementazione di mappe cognitive che rappresentano la parte decisionale del nostro modello. **Nel complesso, CogTrust integra diverse tecnologie Java per realizzare di una completa architettura simulativa per lo studio dell'evoluzione della fiducia all'interno di societa artificiali.**

**JFCM** Come siete venuti a conoscenza di JFCM? Che ruolo ha la libreria in CogTrust? Come è stata utilizzata?

**CogTrust** Abbiamo scoperto JFCM googlando _"FCM Java"_. In realtà, il nostro modello ha alla base meccanismi basati su [Mappe Cognitive Fuzzy (FCM)](en.wikipedia.org/wiki/Fuzzy_cognitive_map), uno strumento di modellazione diffuso in Psicologia per modellare relazioni causali tra concetti per mezzo di logica fuzzy. Avevamo quindi bisogno di utilizzare all'interno di CogTrust un framework Java che realizzasse in pieno le funzionalita' di supporto per lavorare con FCM, ed abbiamo pensato di partire da un progetto gia consolidato.

I vantaggi che hanno portato alla scelta di JFCM tra varie altre librerie sono stati la qualita' di essere open source, di fornire una adeguata documentazione, esempi facilmente eseguibili, con una struttura semplice e sufficientemente flessibile da estendere.

L'evoluzione del nostro progetto ha portato la libreria JFCM ad essere ampliata con nuove funzionalita'che hanno riguardato soprattutto la parte di apprendimento automatico. **Il nostro modello è fra i primi al mondo ad aver applicato a livello multi agente il criterio di machine learning alle mappe.** Inoltre abbiamo studiato nuovi criteri di convergenza nelle reti cognitive, testando e valutando il nostro approccio verso meccanismi largamente diffusi come algoritmi di data mining e reti neurali. Questa estensione e' chiamata [Learning-JFCM](http://sourceforge.net/projects/mindraces-bdi/) ed e' tutt'ora presente e disponibile come open source all'interno di CogTrust.

**JFCM** Pensate di utilizzarla per altri progetti?

**CogTrust** La nostra attività è sempre guidata dalle sfide provenienti dal mondo della ricerca su Trust. Un dominio altamente multidisciplinare, cui afferiscono studi piu disparati, dai rami accademici a quelli socio economici e politici, con contributi provenienti da ogni angolo del mondo. Per questo ritengo che ulteriori progetti per lo sviluppo di tecnologie FCM all'interno di modelli cognitivi, non potranno prescindere dal lavoro fatto in CogTrust, nel quale JFCM svolge un ruolo fondamentale.

**JFCM** JFCM è un progetto relativamente giovane e il sottoscritto è (al momento) l'unico sviluppatore, sicuramente ci sono ampi margini di miglioramento. Avete avuto qualche difficoltà particolare nell'utilizzo? Avete qualche consiglio da dare o vi piacerebbe vedere implementata qualche funzionalità particolare?

**CogTrust** Ritengo che la qualità principale di JFCM sia quella di realizzare uno strumento sufficientemente generico e flessibile per chiunque voglia approcciare l'uso delle mappe cognitive a livello di programmazione Java. Il problema piuttosto riguarda l'applicazione delle FCM come meccanismo di progettazione e decision making: ci siamo accorti, nostro malgrado, che **calibrare il funzionamento di una FCM puo' essere una impresa molto ardua**, a fronte di problemi complessi. Le dinamiche di una FCM sono solo parzialmente traducibili con concetti e relazioni traducibili simbolicamente. Al crescere della complessità si fanno preponderanti altri effetti legati alla topologia delle reti, alle situazioni contingenti, all'ambiente di funzionamento. Questo ci ha portato ad esempio ad un lungo studio degli effetti delle varie funzioni di trasferimento da utilizzare nei nodi (identità, sigmoidi, tangenti iperboliche, tutte ampiamente parametrizzabili). La scelta della particolare funzione non è mai banale: la nostra esperienza ci porta a ipotizzare una stretta correlazione con i vari domini di riferimento, con la topologia delle reti etc. **Una delle conclusioni del nostro contributo scientifico è che senza un solido meccanismo di apprendimento le FCM possano difficilmente essere adottate in applicazioni concrete.** Ovvero, io non le userei per guidare un auto o controllare un robot che va su Marte.

Il consiglio e' di mantenere JFCM in continuo e costante aggiornamento sulle ultime tecniche che emergono e emergeranno dallo studio di FCM, ad esempio in fatto di machine learning. Un occhio attento deve andare anche a tecnologie affini al modello delle FCM, come reti neurali o reti Bayesiane, che potrebbero essere integrate nel progetto JFCM con importanti margini di miglioramento.

**JFCM** Volete aggiungere qualcosa?

**CogTrust** JFCM ha contribuito ai successi raggiunti nella nostra ricerca che hanno permesso di pubblicare vari articoli scentifici. Tra questi citiamo:

- Falcone, R., Piunti, M., Venanzi, M. and Castelfranchi, C. (2011)  
    [_From Manifesta to Krypta: The Relevance of Categories for Trusting Others. ACM Transactions on Intelligent Systems and Technology_](http://eprints.ecs.soton.ac.uk/22206/);
- Venanzi, M., Piunti, M., Falcone, R. and Castelfranchi, C. (2011)  
    [_Facing Openness with Socio Cognitive Trust and Categories. In: 22nd International Joint Conference on Artificial Intelligence (IJCAI), 16-22 July 2011, Barcelona, Spain. pp. 400-405_](http://eprints.ecs.soton.ac.uk/22204/)
- Venanzi, M., Piunti, M., Falcone, R. and Castelfranchi, C. (2011)  
    [_Reasoning with Categories for Trusting Strangers: a Cognitive Architecture. In: 14th International Workshop on Trust in Agent Societies at AAMAS 2011 , May 2011, Taipei_](http://eprints.ecs.soton.ac.uk/22112/)

Spero che questo possa essere uno lo stimolo, e anche un augurio, per gli sviluppatori di JFCM per lavorare su questo progetto con ancor più entusiasmo.

**Matteo Venanzi**  
Former member of the T3 group, ISTC  
PhD Student,  
Electronics and Computer Science,  
University of Southampton.  
UK

**Michele Piunti, PhD**  
Former member of the T3 group, ISTC ;  
Dottorato in Ingegneria Infromatica Elettronica e delle Telecomunicazioni, Università di Bologna - DEIS;  
Attualmente consulente IT presso Whitehall Reply.

### Link

- [Trust, Theory and Technology (t3.istc.cnr.it)](http://t3.istc.cnr.it/);
- [Homepage del progetto _CogTrust_](http://t3.istc.cnr.it/trustwiki/index.php/Cog-Trust);
- altre tecnologie utilizzate nel progetto:
    - [_Jason_](http://jason.sourceforge.net/);
    - [piattaforma _CartAgO_](http://cartago.sourceforge.net/);
    - [JGraph](http://www.jgraph.com/);
- [Mappe Cognitive Fuzzy (FCM)](en.wikipedia.org/wiki/Fuzzy_cognitive_map);
- [Learning-JFCM](http://sourceforge.net/projects/mindraces-bdi/).
