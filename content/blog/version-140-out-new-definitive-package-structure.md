---
title: "Version 1.4.0 is out - new definitive package structure"
date: 2012-10-25T16:10:35+02:00
---

[As already announced before](http://jfcm.megadix.it/blog/new-release-133-new-features-plus-new-14-snapshot), versions from 1.4.x on will have a new package structure:

- moved `ConceptActivator` and `FcmConncetion` implementations in sub-packages:
   - `org.megadix.jfcm.act` : `ConceptActivator` implementations;
   - `org.megadix.jfcm.conn` : `FcmConnection` implementations;

Moreover, _**Visitor**_ **design pattern** was implemented to ease the development of serializers (XML, JSON, etc.), map analyzers, etc.
