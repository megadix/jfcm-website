---
title: "About 1.0-SNAPSHOT, 2.0-SNAPSHOT, and branching"
date: 2011-12-24T15:21:41Z
---

Dear users, I want to share with you some thought about the latest developments in JFCM library.

Recently i started a **huge refactoring** to the core library, refactoring that will (hopefully) make JFCM more generic and transform it to something like a "standard API" for fuzzy cognitive maps. I know this sounds a bit optimistic, but I think we're not so far from this; JFCM was designed from the beginning to be as general, extensible, and flexible as possible, this re-design is just going further in that direction.

But before diving into details, let me reassure you: **I won't stop fixing bugs on 1.x** versions; to make this obvious I've created two new branches on SVN: 1.0-SNAPSHOT and 2.0-SNAPSHOT.

Here's a summary of the changes from 1.x to 2.x:

- CognitiveMap, Concept and FcmConnection are now interfaces;
- base implementations of CognitiveMap, Concept and FcmConnection, previously found in 1.x, are now in org.megadix.jfcm.impl package;
- a new "entity storage" mechanism is under work, to allow various persistence engines (e.g. Database, Graph & NoSQL DBs, ecc.) to handle map entities;
- removal of Apache Commons Lang dependency;
- ... many other!

Any comments and suggestions are of course *very* encouraged!
