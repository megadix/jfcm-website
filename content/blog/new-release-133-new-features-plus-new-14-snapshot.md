---
title: "New release 1.3.3 with new features, plus new 1.4-SNAPSHOT"
date: 2012-10-22T16:07:30+02:00
---

### Release 1.3.3

-   `CognitiveMap.reset()`: do not set `output` or `fixedOutput` if fixedOutput true
- `Concept.startUpdate()`: bugfix if `conceptActivator == null`;
-   `FcmIO.loadXml(String filename)`;
-   fixed comment in `BaseConceptActivator`
-   Implemented `CognitiveMap.description`:
    -   `CognitiveMap`: added description property;
    -   `FcmIO`: load / save
-   Concept activator:
    -   bug fixes and enhancements;
    -   **new ConceptActivator implementations!**
        -   `CauchyActivator`;
        -   `GaussianActivator`;
        -   `IntervalActivator`;
        -   `NaryActivator`;
    -   updated XSD and namespace: [http://www.megadix.org/standards/JFCM-map-v-1.2.xsd](http://www.megadix.org/standards/JFCM-map-v-1.2.xsd "http://www.megadix.org/standards/JFCM-map-v-1.2.xsd")
-   pull up `checkValues()` method to `AbstractConceptActivatorTest`;
    -   `Concept.setFixedOutput()`: changed `Boolean` (Object, may be null) to `boolean` (native, never null)
-   better `toString()` methods: `CognitiveMap`, `Concept`, `WeightedConnection`.

Go grab your copy and experiment!

### Release 1.4-SNAPSHOT breaks (a bit) compilability

This new 1.4.x branch is going to be a bit disruptive because of **some changes in packages organization**. This will break compilation of most users, but the transition is really easy because there are no (yet) changes to functionality:

-   moved ConceptActivator and FcmConncetion implementations in **new sub-packages**:
    -   **org.megadix.jfcm.act** : ConceptActivator implementations;
    -   **org.megadix.jfcm.conn** : FcmConnection implementations;
