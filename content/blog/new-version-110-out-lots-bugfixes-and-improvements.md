---
title: "New version 1.1.0 is out with lots of bugfixes and improvements"
date: 2012-04-06T15:27:20Z
---

This release ha both improvements and bugfixes:

- moved threshold to BaseConceptActivator;
- bugfix: threshold was not used in transfer functions!
- added LinearConceptActivator;
- **modification in XML format:**
  - <param> elements now enclosed in <params> element;
  - XSD namespace fix
  - version 1.0 became `http://www.megadix.org/standards/JFCM-map-v-1.0.xsd` (added .xsd suffix)
  - version 1.1 (current): `http://www.megadix.org/standards/JFCM-map-v-1.1.xsd`
  - modification of FcmIO;
  - many new JUnit tests, both for modifications and bugfixes
