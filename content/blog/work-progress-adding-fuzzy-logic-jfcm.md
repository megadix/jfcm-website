---
title: "Work in progress / adding Fuzzy Logic to JFCM"
date: 2011-03-31T13:28:17Z
categories: ["News"]
tags: ["jfcm-fuzzy", "tools", "libraries", "fuzzy logic", "projects", "jFuzzyLogic"]
---

### Main development

Recently I was able to work on a more regular basis on JFCM-core, so I could start fixing the most important bugs. You can see it also from the [increased number of SVN commits](https://sourceforge.net/project/stats/detail.php?group_id=282469&ugn=jfcm&type=svn), mainly on _JFCM-core_ module.

My main goal in the short period is to **fix all outstanding bugs** so we have a _**"Generally Available" (GA)**_** version** that could be used, tested, analyzed, optimized, etc. in the _real world_, for _real problems_.

### New library: JFCM-fuzzy

In the meantime I was able to pick another interesting project, [**_jFuzzyLogic_**](http://jfuzzylogic.sourceforge.net/), and use it to create a JFCM extension: **_JFCM-fuzzy_**.

In short, _JFCM-fuzzy_ will:

-   enable **evaluation of concepts and connections using fuzzy rules**;
-   rules can be entered in [**_Fuzzy Control Language (FCL)_**](http://en.wikipedia.org/wiki/Fuzzy_Control_Language), thanks to _jFuzzyLogic_.

I did some experiments and results are encouraging, soon I will be able to commit everything to SVN.

_Stay tuned!_

Dimitri
