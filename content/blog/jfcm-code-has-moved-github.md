---
title: "JFCM Code has moved to GitHub!"
date: 2014-12-22T16:15:05+02:00
---

Hello JFCM users,

I was finally able to migrate from SVN to GIT, and to move code **from SourceForge to GitHub**.

Unfortunately the "Import from SVN" script of GitHub is not perfect, so I decided to upload the current code, as-is. This unfortunately means that code history is broken, so if, for whatever reason, you're interested in how the code evolved, you must still reference SourceForge SVN repository:

[https://sourceforge.net/p/jfcm/code/HEAD/tree/](https://sourceforge.net/p/jfcm/code/HEAD/tree/)

The address of the new repository is:

[https://github.com/megadix/jfcm](https://github.com/megadix/jfcm)

Please report any inconsistencies you may find (in the code or in the website) directly to me:

[mailto:info@fcmplayground.com](mailto:info@fcmplayground.com)
