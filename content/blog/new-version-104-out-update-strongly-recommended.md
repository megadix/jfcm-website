---
title: "New version 1.0.4 is out, update is STRONGLY recommended!"
date: 2011-03-16T13:21:43Z
categories: ["News"]
tags: ["release", "algorithm", "bugfix"]
---

I had the time to finally fix the most important bug in JFCM.

In short: **calculation of concept outputs, up to version 1.0.3, was incorrect!**

Now `BaseConceptActivator.activate()` uses a two-step algorithm:

1. recalculates incoming connections' output;
- calculates Concept's output.

Go [download](/download) it!
