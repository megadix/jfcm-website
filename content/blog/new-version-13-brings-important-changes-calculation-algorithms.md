---
title: "New version 1.3 brings important changes to calculation algorithms"
date: 2012-07-10T16:18:44+02:00
---

Release 1.3 changelog:

-   `BaseConceptActivator.includePreviousOutput`;
-   change in output calculations: `HyperbolicTangentActivator`, `LinearActivator`, `SigmoidActivator`, `SignumActivator` include by default (`includePreviousOutput = true`) previous output in calculations;
-   #17 Remove Jakarta Commons dependency, added `StringUtils` as substitute.

### What has changed in calculations and algorithms, and why

Recently I've been in contact with [Dr. Elpiniki I. Papageorgiou](http://epapageorgiou.com/), I am so grateful to her because she gave me the opportunity to **better understand theory of cognitive maps** and **learning algorithms**, too! Dr. Papageorgiu is organizing a **new book about fuzzy cognitive maps**, and if everything goes well there will be **a chapter about JFCM**. Now that's exciting! However, this means that the library calculations must be perfect, at least from an "academic" point of view, so I had to study a lot of material, especially regarding learning algorithms. After all this effort it was clear to me that the default calculations (until version 1.2-SNAPSHOT of JFCM), were not 100% accurate. The problem is that default ConceptActivator implementations used to calculate next output based only on incoming connections, completely ignoring previous value.

Now you have two possibilies:

-   default implementations include previous value of `Concept` during calculation of `Concept.nextOutput`;
-   you still can use the old algorithm, calling **activator.setIncludePreviousOutput(false)**.

**This change is going to break some applications**, but the call above to setIncludePreviousOutput() should solve any compatibility issues.

### Support material

In the _[Journals](http://epapageorgiou.com/?q=Journals)_ sections of Dr. Papageorgiou's website you can find a lot of support material, the most important article being (for the above modifications):

E.I. Papageorgiou, C.D. Stylios & P.P. Groumpos, _"Unsupervised learning techniques for fine-tuning Fuzzy Cognitive Map causal links"_, Intern. Journal of Human-Computer Studies, Elsevier, vol. 64, 2006, pp. 727-743.
