---
title: "Cogtrust Interview English"
date: 2012-01-03T15:24:07Z
---

[_CogTrust_](http://t3.istc.cnr.it/trustwiki/index.php/Cog-Trust) is a research project that uses JFCM for the "intelligent" part of its model. It is an example of how multi-disciplinarity and the use of available open source technologies can bring great results and enhancements to the tools themselves.

Reading their answers, as developer and "dad" of JFCM I felt really proud, but the most important thing is that I cannot wait to get my hands on JFCM code again because I'm full of new ideas!

[![](/system/files/u1/it.png) Clicca qui per l'intervista originale (Italiano)](http://jfcm.megadix.it/blog/intervista-cogtrust-italiano)

### The Interview

**JFCM** Please Introduce yourself; let's talk about the team: where it was born, past experiences, ongoing projects.

**CogTrust** Our team is called T3: [Trust, Theory and Technology (t3.istc.cnr.it)](http://t3.istc.cnr.it/), it is a research group within _Istituto di Scienze e Technologie Cognitive_ \- CNR (National Center of Research) of Rome. T3 research interests are mainly about **trust research inside social domains, particularly focused in the study of socio-cognitive models related to Trust**.

Born around year 2000, our group did many collaborations with international researchers from various scientific sectors as Psychology, Sociology, Information Technology and Artificial Intelligence. It already well-known in the international scientific community, with over 100 publications on most important journals and conferences in the field. Moreover, our group is also active in many community initiatives such as workshops and editorial activities, with the objective of stimulating and support research on Trust.

**JFCM** What is exactly CogTrust project?

**CogTrust** [_CogTrust_](http://t3.istc.cnr.it/trustwiki/index.php/Cog-Trust) is **a software project developed in a research effort about trust-based socio-cognitive computational processes**. CogTrust implements a trust evaluation model with rules and categories that is based on trust theory by Falcone et al. This theory explains well how trust grows in the mind of a cognitive agent and how this has a crucial role in delegation and cooperation processes (for more details, see _Trust Theory: A Socio-Cognitive and Computational Model; Castelfranchi, Falcone, Wiley, 2010_).

For its purpose, CogTrust realizes an infrastructure that simulates a social system where agents (_trustors_) try to execute their own tasks leveraging cooperation and delegation. The system studies the decision making process based on trust, like (for example) the delegation of some of the activities to other agents, chosen among the population of possible executors (trustee). The claim of the model is that the evaluation of the choice of delegation criteria happens through strategies based on trust, influenced by catagorization mechanisms, risk evaluation and various operative contexts. From this perspective, CogTrust is a decisional mechanism that makes exploration of information possibile, making it available and searchable also by simple software "robots".

**JFCM** Which technologies did you choose for the project?

**CogTrust** CogTrust is a completely open source project. The glue technology is Java but in CogTrust there are many libraries and advanced programming tecniques. Being more specific, CogTrust implements a Multi-Agent system: [_Jason_](http://jason.sourceforge.net/) is the language used for deliberative mechanisms of the agents, [_CartAgO_](http://cartago.sourceforge.net/) is the platform used for simulation environment programming, [JGraph](http://www.jgraph.com/) for the graphic part, analysis and visualization of results by means of interactive graphs, and of course [JFCM](http://jfcm.megadix.it/) for the cognitive maps implementation, maps that represent the decisional part of the model. **Overall, CogTrus integrates various Java technologies to realize a complete simulative architecture for the study of trust evolution in artificial societies.**

**JFCM** How did you get to know JFCM? Which role has the library in CogTrust? How is it used?

**CogTrust** We discovered JFCM simply googling for _"FCM Java"_. Our model actually is based on [Fuzzy Cognitive Maps(FCM)](en.wikipedia.org/wiki/Fuzzy_cognitive_map), a tool frequently used in Psychology to model causal relations between concepts using fuzzy logic. In CogTrust we needed to use a Java framework that fully implements FCMs, so we decided to start with an already consolidated project.

Advantages of JFCM among other libraries are its open source nature, its adequate documentation, easy to run examples, and a simple structure easy to extend.

- - -

The evolution of our project led an evolution of JFCM itself with new functionality, especially for automatic learning. **Our own model is among the first worldwide that applies maps machine learning at multi-agent level.** Moreover, we studied new convergence criteria in cognitive nets, testing and evaluating our own approach toward popular techniques as data mining algorithms and neural nets. This extension is called [Learning-JFCM](http://sourceforge.net/projects/mindraces-bdi/) and is available as open source from CogTrust.

**JFCM** Do you think you will use JFCM for other projects?

**CogTrust** Our activity is always driven by challenges coming from research about Trust. A quite multi-disciplinar domain, where the most disparate studies gather, from academic to socio-economic and political, with contributions from every corner if the world. Because of this I think that many new projects for FCM technology development in cognitive models cannot ignore the work done in CogTrust, where JFCM has a fundamental role.

**JFCM** JFCM is a relatively new project and I am (at the moment) the only developer, so there is for sure much room for improvement. Did you face any outstanding difficulties during development? Do you have any suggestions or any new functionality you'd like to see implemented?

**CogTrust** One of the prominent qualities of JFCM is to be a tool enough general and flexible for anyone who wants to approach cognitive maps with Java. The actual problem would be instead the application of FCMs themselves as design and decision making mechanism: despite our best intentions we found out thath **calibrating an FCM configuration can be a really tough task**, given complex problems. FCM dynamics are only partially translatable to symbolic concepts and relations. With complexity grow also side effects, due mainly to net topology, actual system state, environment. This led us for example to a long study about effects of the various node transfer functions (Identity, Sigmoid, Hyperbolic Tangent, all broadly parametrizable). The choice of a particular function is never trivial: our hypothesis, based on actual experience, is that there is a close correlation with the various reference domains involved, net topology, etc. **One of the outcome of our scientific contribution is that without a proper learning mechanism FCMs can hardly be used in concrete applications**. This means that, well, I would NOT use them to drive a robot to Mars!

Our advice is to keep developing JFCM and to update it with the new FCM techniques that will eventually arise, for example in machine learning. An eye should also be kept on technologies similar to FCMs, like neural nets and Bayesian nets, that could be integrated in JFCM project to make it better.

**JFCM** Would you like to add something?

**CogTrust** JFCM contributed to the success of our research, success that permitted us the publishing of various scientific articles. Here are some of them:

-   Falcone, R., Piunti, M., Venanzi, M. and Castelfranchi, C. (2011)  
    [_From Manifesta to Krypta: The Relevance of Categories for Trusting Others. ACM Transactions on Intelligent Systems and Technology_](http://eprints.ecs.soton.ac.uk/22206/);
-   Venanzi, M., Piunti, M., Falcone, R. and Castelfranchi, C. (2011)  
    [_Facing Openness with Socio Cognitive Trust and Categories. In: 22nd International Joint Conference on Artificial Intelligence (IJCAI), 16-22 July 2011, Barcelona, Spain. pp. 400-405_](http://eprints.ecs.soton.ac.uk/22204/)
-   Venanzi, M., Piunti, M., Falcone, R. and Castelfranchi, C. (2011)  
    [_Reasoning with Categories for Trusting Strangers: a Cognitive Architecture. In: 14th International Workshop on Trust in Agent Societies at AAMAS 2011 , May 2011, Taipei_](http://eprints.ecs.soton.ac.uk/22112/)

We hope that this could be a stimulus, and also a good wish, for JFCM developers to work on this project with more enthusiasm.

**Matteo Venanzi**  
Former member of the T3 group, ISTC  
PhD Student,  
Electronics and Computer Science,  
University of Southampton.  
UK

**Michele Piunti, PhD**  
Former member of the T3 group, ISTC ;  
Dottorato in Ingegneria Infromatica Elettronica e delle Telecomunicazioni, Università di Bologna - DEIS;  
Attualmente consulente IT presso Whitehall Reply.

### Link

-   [Trust, Theory and Technology (t3.istc.cnr.it)](http://t3.istc.cnr.it/);
-   [_CogTrust_ project homepage](http://t3.istc.cnr.it/trustwiki/index.php/Cog-Trust);
-   other technologies involved:
    -   [_Jason_](http://jason.sourceforge.net/);
    -   [_CartAgO_ platform](http://cartago.sourceforge.net/);
    -   [JGraph](http://www.jgraph.com/);
-   [Fuzzy Cognitive Maps (FCM)](en.wikipedia.org/wiki/Fuzzy_cognitive_map);
-   [Learning-JFCM](http://sourceforge.net/projects/mindraces-bdi/).

- - -
