---
title: "New release bugfix release 1.3.2"
date: 2012-07-04T16:02:40+02:00
---

[Version **1.3.2** is out!](https://sourceforge.net/projects/jfcm/files/releases/1.3.2/)

- bugfix to BaseConceptActivator.calculateNextOutput() on null or NaN inputs;
- `SignumActivator`:
    - added `SignumActivator.Mode` enum: BIPOLAR (default) and BINARY;
    - added zeroValue property, to let users customize value at zero input;
- update FcmIO to load and save SignumActivator.mode;
- `Concept`: implemented equals() / hashCode();
- small fix to SimpleFcmRunner
- `BaseConceptActivator`: check threshold parameter;
- simpler and easier tests for concept activators.
