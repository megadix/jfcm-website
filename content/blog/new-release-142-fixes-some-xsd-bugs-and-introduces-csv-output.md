---
title: "New release 1.4.2 fixes some XSD bugs and introduces CSV output"
date: 2014-12-27T16:16:43+02:00
---

New version of JFCM is out: **1.4.2**

Here is the changelog from previous version:

### Release 1.4.2

-   fixed XSD:
    -   moved `CAUCHY`, `GAUSS`, `INTERVAL`, `LINEAR`, `NARY` activator enum to `JFCM-map-v-1.2.xsd`
-   FcmRunner:
    -   added method to `FcmRunner` interface: `void run()`;
    -   introduced `BaseFcmRunner` to ease development of subclasses;
    -   added CSV output functionality to `SimpleFcmRunner`
-   changed format of `README` from Textile to Markdown

### Download of binaries and source code

As always, releases are available from **Sourceforge**:

[http://sourceforge.net/projects/jfcm/files/latest/download](http://sourceforge.net/projects/jfcm/files/latest/download)

Source code instead has been moved to GitHub:

[https://github.com/megadix/jfcm/releases/tag/v1.4.2](https://github.com/megadix/jfcm/releases/tag/v1.4.2)
