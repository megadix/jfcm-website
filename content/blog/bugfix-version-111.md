---
title: "Bugfix version 1.1.1"
date: 2012-04-19T15:30:06Z
---
New version 1.1.1 fixes several bugs in 1.1.0:

- #1 XML specification enhancements;
- #3 Example XML files should have namespace location
- #4 WeightedConnection.calculateOutput() throws NullPointerException
- #5 better CognitiveMap.toString()
- #6 Upgrade dependency commons-lang to 3.x
- #10 Use constants for ConceptActivator
- #11 Add support for LinearActivator in FcmIo

Other changes:

- removed year from copyright header in source files;
- renamed LinearConceptActivator to LinearActivator
