---
title: "About"
date: 2018-07-30T13:06:45Z
---

JFCM is a Java library that implements [Fuzzy Cognitive Maps (FCM)](http://en.wikipedia.org/wiki/Fuzzy_cognitive_map).
It is small, simple to use and flexible enough to be used as a basis for other projects / libraries.
JFCM is developed by [Dimitri De Franciscis](http://www.megadix.it/).
